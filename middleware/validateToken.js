
import jwt from 'jsonwebtoken';
import jwkToPem from 'jwk-to-pem';
import jwks from '../config/jwk.json';
import { Messages } from '../util';

// eslint-disable-next-line consistent-return
export default (req, res, next) => {
  const jwtToken = req.headers.authorization && req.headers.authorization.split(' ')[1];

  if (!jwtToken) return res.status(401).send({ error: Messages.ERROR_NOT_VALID_TOKEN });

  const decodedToken = jwt.decode(jwtToken, { complete: true });
  const { kid } = decodedToken.header;
  const jwk = jwks.keys.filter(item => kid === item.kid)
    ? jwks.keys.filter(item => kid === item.kid)[0]
    : null;
  const issuer = `https://cognito-idp.${process.env.COGNITO_REGION}.amazonaws.com/${process.env.COGNITO_USER_POOL_ID}`;
  const audience = process.env.COGNITO_CLIENT_ID;

  if (!jwk) return res.status(401).send({ error: Messages.ERROR_INVALID_REQUEST });

  const pem = jwkToPem(jwk);

  jwt.verify(jwtToken, pem, { algorithms: ['RS256'], issuer, audience }, (error) => {
    if (error) {
      return res.status(400).json(error);
    }
    return next();
  });
};
