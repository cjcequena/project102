import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
// eslint-disable-next-line import/no-extraneous-dependencies
import morgan from 'morgan';
import dotenv from 'dotenv';
import middleware from './middleware';
import usersRouter from './routes/users';
import indexRouter from './routes/index';
import authRouter from './routes/auth';
import tripRouter from './routes/trips';

const tripsRouter = require('./routes/trips');

const app = express();

global.fetch = require('node-fetch');


dotenv.config();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/trips', tripsRouter);
app.use('/users', middleware, usersRouter);
app.use('/auth', authRouter);
app.use('/trips', tripRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
