import { AuthenticationDetails, CognitoUser } from 'amazon-cognito-identity-js';
import bcrypt from 'bcryptjs';
import { check, validationResult } from 'express-validator';
import User from '../models/User';
import { Messages, AWS } from '../util';

const express = require('express');

const router = express.Router();

const validation = [
  check('email').isEmail(),
];

// validation middleware
router.use(validation, (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  return next();
});

router.put('/register', (req, res) => {
  const { email, phone, password } = req.body;

  const salt = bcrypt.genSaltSync(10);
  const data = {
    email,
    phone,
    password: bcrypt.hashSync(password, salt),
  };

  return User.create(data)
    .then(result => res.status(200).json(result))
    .catch(error => res.status(400).json({ error }));
});

router.post('/login', (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email }).then((result) => {
    if (!result) {
      return res.status(401).json({ error: Messages.ERROR_INVALID_CREDENTIALS });
    }
    if (!bcrypt.compareSync(password, result.password)) {
      return res.status(400).json({ error: Messages.ERROR_INVALID_CREDENTIALS });
    }
    return res.status(200).json(result);
  }).catch(error => res.status(400).json({ error }));
});

router.post('/cognito/register', (req, res) => {
  const { cognitoAttribute, cognitoUserPool } = AWS;
  const {
    email,
    password,
    phone,
    identityProvider,
  } = req.body;
  const userPool = cognitoUserPool();
  let attributeList;

  try {
    attributeList = [
      cognitoAttribute('email', email),
      cognitoAttribute('phone_number', phone),
      cognitoAttribute('custom:identity_provider', identityProvider),
    ];
  } catch (error) {
    return res.status(500).json({ error });
  }

  return userPool.signUp(email, password, attributeList, null, (error, result) => {
    if (error) {
      return res.status(400).json(error);
    }
    return res.status(200).json(result);
  });
});

router.post('/cognito/login', (req, res) => {
  const { email: Username, password: Password } = req.body;
  const userPool = AWS.cognitoUserPool();

  const authenticationDetails = new AuthenticationDetails({ Username, Password });

  const userData = {
    Username,
    Pool: userPool,
  };

  const cognitoUser = new CognitoUser(userData);

  cognitoUser.authenticateUser(authenticationDetails, {
    // const accessToken = result.getAccessToken().getJwtToken();
    // Use the idToken for Logins Map when Federating User Pools with
    // identity pools or when passing through an Authorization Header to
    //  an API Gateway Authorizer*/
    // const idToken = result.idToken.jwtToken;
    onSuccess: result => res.status(200).json(result),
    onFailure: error => res.status(400).json({ error }),
  });
});

router.post('/cognito/user', (req, res) => {
  const { email: Username, password: Password } = req.body;
  const userPool = AWS.cognitoUserPool();

  const authenticationDetails = new AuthenticationDetails({ Username, Password });

  const userData = {
    Username,
    Pool: userPool,
  };

  const cognitoUser = new CognitoUser(userData);

  cognitoUser.authenticateUser(authenticationDetails, {
    // const accessToken = result.getAccessToken().getJwtToken();
    // Use the idToken for Logins Map when Federating User Pools with
    // identity pools or when passing through an Authorization Header to
    //  an API Gateway Authorizer*/
    // const idToken = result.idToken.jwtToken;
    // onSuccess: result => res.status(200).json(result),
    onSuccess: () => {
      cognitoUser.getUserAttributes((error, result) => {
        if (error) {
          return res.status(400).json(error);
        }
        return res.status(200).json(result);
      });
    },
    onFailure: error => res.status(400).json({ error }),
  });
});

module.exports = router;
